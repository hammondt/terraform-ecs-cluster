locals {
  role_name = "ecs-${var.name}"
}

resource "aws_iam_role" "IamRole" {
  name               = local.role_name
  tags               = var.tags
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_instance_profile" "Ec2InstanceProfile" {
  role = aws_iam_role.IamRole.name
  name = local.role_name
  tags = var.tags
}
resource "aws_iam_role_policy_attachment" "IamRoleManagedPolicyRoleAttachment0" {
  role       = aws_iam_role.IamRole.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
resource "aws_iam_role_policy_attachment" "IamRoleManagedPolicyRoleAttachment1" {
  role       = aws_iam_role.IamRole.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}
resource "aws_iam_role_policy_attachment" "IamRoleManagedPolicyRoleAttachment2" {
  role       = aws_iam_role.IamRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}
