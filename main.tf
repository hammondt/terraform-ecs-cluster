resource "aws_ecs_cluster" "this" {
  name = var.name
  tags = var.tags
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

### Launch template controls the primary stuff about our instances
resource "aws_launch_template" "this" {
  name_prefix            = "${var.name}-asg-"
  instance_type          = var.asg_instance_type
  image_id               = data.aws_ami.ecs_ami.id
  user_data              = base64encode(var.user_data)
  vpc_security_group_ids = [aws_security_group.this.id]
  tags                   = var.tags
  iam_instance_profile {
    name = aws_iam_instance_profile.Ec2InstanceProfile.name
  }
}

## Create a simple security group to allow egress only.
## Ingress can be controlled later on services or as rules
resource "aws_security_group" "this" {
  name   = var.name
  vpc_id = var.vpc_id
  tags   = var.tags
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

### ASG setup. Shouldn't be too much to setup via min/max instances
resource "aws_autoscaling_group" "this" {
  name_prefix           = "${var.name}-asg-"
  max_size              = var.asg_max_size
  min_size              = var.asg_min_size
  vpc_zone_identifier   = var.asg_subnets
  protect_from_scale_in = true

  launch_template {
    id      = aws_launch_template.this.id
    version = "$Latest"
  }
  ### Ensure ECS managed tag is propgated. Required for capacity providers
  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }
  dynamic "tag" {
    for_each = merge(data.aws_default_tags.current.tags, var.tags)
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  ### Setup instance refresh on a rolling basis
  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }
}

# Builds a capacity provider to scale the cluster based on the needs of ECS services
# Meaning, when RAM/CPU is assigned to a service, the cluster will scale based on
# the required load in addition to normal ASG options such as host load
resource "aws_ecs_capacity_provider" "this" {
  name = var.name
  tags = var.tags
  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.this.arn
    managed_termination_protection = "ENABLED"
    managed_scaling {
      status = "ENABLED"
    }
  }
}

# Assigns the capacity provider to the cluster
resource "aws_ecs_cluster_capacity_providers" "this" {
  cluster_name       = aws_ecs_cluster.this.name
  capacity_providers = [aws_ecs_capacity_provider.this.name]
  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.this.name
    base              = 1
    weight            = 100
  }
}
