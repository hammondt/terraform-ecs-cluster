variable "name" {}
variable "asg_max_size" {
  type        = number
  description = "ASG Max Instance Size"
  default     = 4
}
variable "asg_min_size" {
  type        = number
  description = "ASG Min Instance Size"
  default     = 0
}
variable "asg_instance_type" {
  type        = string
  description = "ASG instance type"
  default     = "t3a.medium"
}
variable "asg_subnets" {
  type        = list(string)
  description = "Subnets for cluster hosts"
}
variable "vpc_id" {
  description = "VPC the cluster lives in"
}
variable "tags" {
  default     = {}
  description = "Tags to apply to resources"
}

variable "user_data" {
  default = null
}
